import json
import time
from cv2 import imread
# import a model_api from model_api.py
import model_api
import os
import gc


class ModelHandler(object):

    def __init__(self):
        self.model_version = '1.0.0'

    def CleanUp(self):
        free_m = gc.collect()
        print("free memory: ", free_m)
        os.remove(img_name)


    def on_post(self, req, resp, **kwargs):
        try:
            path = os.getcwd()
            start_time = time.time()
            img_stream = req.get_param('image_data')
            image = img_stream.file.read()
            img_ext = req.get_param('image_ext')
            img_id = req.get_param('id')
            img_name = path+'/images/'+str(img_id)+'_'+str(time.time())+'.'+img_ext
            with open(img_name,'wb') as f:
                f.write(image)
            # plugin model for prediction
            prediction = model_api.predict(img_name)
            print("Total Time: {}".format(time.time()-start_time))
            resp.body = json.dumps({'status':200,'message':'successful','prediction': prediction,'model_version':self.model_version})
            self.CleanUp()

        except Exception as e:
            print(e)
            resp.body = json.dumps({'status':500,'message':str(e)})
            os.remove(img_name)
